﻿using EmployeeManagement.Models;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace BlazorApp.Services
{
    public class DepartmentService
    {
        private readonly HttpClient httpClient;
        public Department[] Departments { get; private set; }
        public DepartmentService()
        {
        }
        public DepartmentService(HttpClient httpClient)
        {
            this.httpClient = httpClient;
        }
        public async Task<Department> GetDepartment(int departmentId)
        {
            try
            {
                Department emp = null;
                HttpClientHandler handler = new HttpClientHandler();
                handler.ServerCertificateCustomValidationCallback = ServerCertificateCustomValidation;
                HttpClient client = new HttpClient(handler);
                try
                {
                    //HttpResponseMessage response = await client.GetAsync("https://localhost:44334/api/Employees");
                    //response.EnsureSuccessStatusCode();
                    //string responseBody = await response.Content.ReadAsStringAsync();
                    emp = await client.GetJsonAsync<Department>("https://localhost:44334/api/Department/{departmentId}");
                }
                catch (HttpRequestException e)
                {
                    throw e;
                }
                handler.Dispose();
                client.Dispose();
                return emp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public async Task<Department[]> GetDepartments()
        {
            try
            {
                Department[] emp = null;
                HttpClientHandler handler = new HttpClientHandler();
                handler.ServerCertificateCustomValidationCallback = ServerCertificateCustomValidation;
                HttpClient client = new HttpClient(handler);
                try
                {
                    //HttpResponseMessage response = await client.GetAsync("https://localhost:44334/api/Employees");
                    //response.EnsureSuccessStatusCode();
                    //string responseBody = await response.Content.ReadAsStringAsync();
                    emp = await client.GetJsonAsync<Department[]>("https://localhost:44334/api/Department");
                }
                catch (HttpRequestException e)
                {
                    throw e;
                }
                handler.Dispose();
                client.Dispose();
                return emp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private bool ServerCertificateCustomValidation(HttpRequestMessage arg1, X509Certificate2 arg2, X509Chain arg3,
           SslPolicyErrors arg4)
        {
            return true;
        }
    }
}
