﻿using EmployeeManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Services
{
    public interface IDepartmentService
    {
        Task<Department[]> GetDepartments();
        Task<Department> GetDepartment(int departmentId);

    }
}
