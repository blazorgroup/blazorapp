﻿using EmployeeManagement.Models;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Threading.Tasks;

namespace BlazorApp.Services
{
    public class EmployeeService : IEmployeeService
    {

        private HttpClient _client;
        public EmployeeService()
        {
        }
        public EmployeeService(HttpClient client)
        {
            _client = client;
        }
        public Employee[] Employees { get; private set; }
        public async Task<IEnumerable<Employee>> GetEmployees()
        {
            try
            {
                Employee[] emp = null;
                HttpClientHandler handler = new HttpClientHandler();
                handler.ServerCertificateCustomValidationCallback = ServerCertificateCustomValidation;
                HttpClient client = new HttpClient(handler);
                try
                {
                    //HttpResponseMessage response = await client.GetAsync("https://localhost:44334/api/Employees");
                    //response.EnsureSuccessStatusCode();
                    //string responseBody = await response.Content.ReadAsStringAsync();
                    emp = await client.GetJsonAsync<Employee[]>("https://localhost:44334/api/Employees");
                }
                catch (HttpRequestException e)
                {
                    throw e;
                }
                handler.Dispose();
                client.Dispose();
                return emp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private bool ServerCertificateCustomValidation(HttpRequestMessage arg1, X509Certificate2 arg2, X509Chain arg3,
            SslPolicyErrors arg4)
        {
            return true;
        }

        public async Task<Employee> GetEmployeesById(int Id)
        {
            try
            {
                Employee emp = null;
                HttpClientHandler handler = new HttpClientHandler();
                handler.ServerCertificateCustomValidationCallback = ServerCertificateCustomValidation;
                HttpClient client = new HttpClient(handler);
                try
                {
                    //return await client.GetJsonAsync<Employee>($"api/employees/{Id}");
                    emp = await client.GetJsonAsync<Employee>($"https://localhost:44334/api/Employees/{Id}");
                }
                catch (HttpRequestException e)
                {
                    throw e;
                }
                handler.Dispose();
                client.Dispose();
                return emp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            //return await httpClient.GetJsonAsync<Employee[]>("api/employees/{Id}");
        }

        public async Task<Employee> UpdateEmployee(Employee updatedEmployee)
        {
            try
            {
                Employee emp = new Employee();
                HttpClientHandler handler = new HttpClientHandler();
                handler.ServerCertificateCustomValidationCallback = ServerCertificateCustomValidation;
                HttpClient client = new HttpClient(handler);
                try
                {
                    //await client.PutJsonAsync($"https://localhost:44334/api/employees/{updatedEmployee.EmployeeId}", updatedEmployee);
                    await client.SendJsonAsync(HttpMethod.Put, $"https://localhost:44334/api/Employees/{updatedEmployee.EmployeeId}", updatedEmployee);
                }
                catch (Exception ex)
                {
                    throw ex;
                }

                handler.Dispose();
                client.Dispose();
                return emp;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public async Task<Employee> CreateEmployee(Employee newEmployee)
        {
            Employee emp = new Employee();
            HttpClientHandler handler = new HttpClientHandler();
            handler.ServerCertificateCustomValidationCallback = ServerCertificateCustomValidation;
            HttpClient client = new HttpClient(handler);
            try
            {
                emp = await client.PostJsonAsync<Employee>($"https://localhost:44334/api/employees", newEmployee);
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return emp;
        }

        public async Task DeleteEmployee(int EmpId)
        {
            HttpClientHandler handler = new HttpClientHandler();
            handler.ServerCertificateCustomValidationCallback = ServerCertificateCustomValidation;
            // HttpClient client = new HttpClient(handler);
            _client = new HttpClient(handler);
            try
            {
                //HttpResponseMessage response = await client.DeleteAsync($"https://localhost:44334/api/employees/{EmpId}");
                //response.EnsureSuccessStatusCode();
                //string responseBody = await response.Content.ReadAsStringAsync();
                await _client.DeleteAsync($"https://localhost:44334/api/employees/{EmpId}");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
