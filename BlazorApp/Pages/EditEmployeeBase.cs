﻿using BlazorApp.Services;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlazorApp.Utility;
using AutoMapper;
using EmployeeManagement.Models;

namespace BlazorApp.Pages
{
    public class EditEmployeeBase : ComponentBase
    {
        public EditEmployeeBase(IEmployeeService employeeService, IDepartmentService departmentService)
        {
            EmployeeService = employeeService;
            DepartmentService = departmentService;
        }

        public EditEmployeeBase()
        {
        }

        [Inject]
        public IMapper Mapper { get; set; }
        public string PageHeader { get; private set; }
        public Employee Employee { get; set; } = new Employee();
        public IEnumerable<Employee> enumemp { get; set; }
        public EditEmployeeModel editEmployeeModel { get; set; } =
            new EditEmployeeModel();
        [Inject]
        public IEmployeeService EmployeeService { get; set; }
        [Parameter]
        public string Id { get; set; }
        [Inject]
        public IDepartmentService DepartmentService { get; set; }
        public List<Department> Departments { get; set; } = new List<Department>();
        public string DepartmentId { get; set; }

        [Inject]
        public NavigationManager NavigationManager { get; set; }
        protected async override Task OnInitializedAsync()
        {

            int.TryParse(Id, out int employeeId);

            if (employeeId != 0)
            {
                PageHeader = "Edit Employee";
                Employee = await EmployeeService.GetEmployeesById(int.Parse(Id));
                Departments = (await DepartmentService.GetDepartments()).ToList();
                DepartmentId = Employee.DepartmentId.ToString();
            }
            else
            {
                PageHeader = "Create Employee";
                Employee = new Employee
                {
                    DepartmentId = 1,
                    DateOfBirth = DateTime.Now,
                    PhotoPath = "images/nophoto.jpg"
                };
            }

            //Departments = (await DepartmentService.GetDepartments()).ToList();
            //Mapper.Map(Employee, editEmployeeModel);
            //enumemp = await EmployeeService.GetEmployees();
            //Employee = await EmployeeService.GetEmployeesById(int.Parse(Id));
            //Departments = (await DepartmentService.GetDepartments()).ToList();
            //DepartmentId = Employee.DepartmentId.ToString();

        }

        protected async void HandleValidSubmit()
        {
            try
            {
                Mapper.Map(editEmployeeModel, Employee);
                Employee result = null;
                if (Employee.EmployeeId != 0)
                {
                    result = await EmployeeService.UpdateEmployee(Employee);
                }
                else
                {
                    result = await EmployeeService.CreateEmployee(Employee);
                }
                if (result != null)
                {
                    NavigationManager.NavigateTo("/EmployeeLists");
                }
                //Mapper.Map(editEmployeeModel, Employee);
                //var result = await EmployeeService.UpdateEmployee(Employee);
                //if (result != null)
                //{
                //    NavigationManager.NavigateTo("/EmployeeLists");
                //}
            }
            catch (Exception e)
            {

            }
        }
        protected async void Delete_Click()
        {
            try
            {
                await EmployeeService.DeleteEmployee(Employee.EmployeeId);

                NavigationManager.NavigateTo("/EmployeeLists");
            }
            catch (Exception e)
            {

            }
        }

    }
}
