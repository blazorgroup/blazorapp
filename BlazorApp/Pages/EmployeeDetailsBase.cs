﻿using BlazorApp.Services;
using EmployeeManagement.Models;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BlazorApp.Pages
{
    public class EmployeeDetailsBase : ComponentBase
    {
        public Employee Employee { get; set; } = new Employee();
        protected string Name { get; set; } = "Tom";
        protected string Colour { get; set; } = "background-color:green";

        public string Description { get; set; } = string.Empty;

        [Inject]
        public IEmployeeService EmployeeService { get; set; }

        [Parameter]
        public string Id { get; set; }

        protected string Coordinates { get; set; }
        protected string Coordinates1 { get; set; }

        protected void Mouse_Move(MouseEventArgs e)
        {
            Coordinates = $"X = {e.ClientX } Y = {e.ClientY}";
        }
        protected async override Task OnInitializedAsync()
        {

            Id = Id ?? "1";
            Employee = await EmployeeService.GetEmployeesById(int.Parse(Id));
           
        }
    }
}
