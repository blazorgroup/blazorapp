﻿using System;
using System.Threading.Tasks;
using BlazorApp.Services;
using EmployeeManagement.Models;
using Microsoft.AspNetCore.Components;

namespace BlazorApp.Pages
    {
    public class DisplayEmployeeBase : ComponentBase
    {
        [Parameter]
        public Employee Employee { get; set; }

        [Parameter]
        public bool ShowFooter { get; set; }

        protected bool IsSelected { get; set; }

        [Parameter]
        public EventCallback<bool> OnEmployeeSelection { get; set; }

        [Inject]
        public IEmployeeService service { get; set; }
        protected async Task CheckBoxChanged(ChangeEventArgs e)
        {
            IsSelected = (bool)e.Value;
            await OnEmployeeSelection.InvokeAsync(IsSelected);
        }

        protected BlazorApp.Pages.ConfirmBase DeleteConfirmation { get; set; }

        protected void Delete_Click()
        {
            DeleteConfirmation.Show();
        }

        protected async Task ConfirmDelete_Click(bool deleteConfirmed)
        {
            if (deleteConfirmed)
            {
                await service.DeleteEmployee(Employee.EmployeeId);
                //await OnEmployeeDeleted.InvokeAsync(Employee.EmployeeId);
            }
        }

    }
}
