﻿using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EmployeeManagement.Models;
using BlazorApp.Services;

namespace BlazorApp.Pages
{
    public class EmployeeListBase : ComponentBase
    {
        public EmployeeListBase(IEmployeeService employeesservice)
        {
            _employeesservice = employeesservice;
        }
        public EmployeeListBase()
        {

        }
        public IEnumerable<Employee> _employees { get; set; }
        [Inject]
        public IEmployeeService _employeesservice { get; set; }
        public bool ShowFooter { get; set; } = true;

        protected override async Task OnInitializedAsync()
        {
            _employees = (await _employeesservice.GetEmployees());
        }

        protected int SelectedEmployeesCount { get; set; } = 0;

        protected void EmployeeSelectionChanged(bool isSelected)
        {
            if (isSelected)
            {
                SelectedEmployeesCount++;
            }
            else
            {
                SelectedEmployeesCount--;
            }
        }
    }
}
